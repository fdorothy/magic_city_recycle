# Magic City Recycle #

This is the source code for the Magic City Recycling app. This is a web app that lists out easy to use recycling resouces and allows people to participate in recycling!

This was built during the Magic City Hacks 2018.

This is run using firebase. See https://magiccityrecycle.firebaseapp.com/

### Setup ###

TBD

### Contact ###

Send questions to fredricdorothy@gmail.com