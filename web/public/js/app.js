/**
 * Global vars go here
 */
// the firestore db connection
var db = null;
var user = null;
var admin = false;
var addingBin = false;
var addingBinRequest = false;
var markers = {};
var globalMap = null;
var submitLocation = null;
const defaultZoom = 14;

// our mapbox config access token
mapboxgl.accessToken = 'pk.eyJ1IjoidGF5Ym93c2VyIiwiYSI6ImNqa3lnZGQxdzBqN2gzdnFrbWZ4eWNhejIifQ.hK1fAAQHyVbTIc3ccCXi6w';

/**
 *  Initializes our connection to Firebase and
 * initializes the firestore connection
 */
function initializeFirebase() {
  var config = {
    apiKey: "AIzaSyAGuzOA-sfzG_DwjO9sJIQqrfK5eh165K4",
    authDomain: "magiccityrecycle.firebaseapp.com",
    databaseURL: "https://magiccityrecycle.firebaseio.com",
    projectId: "magiccityrecycle",
    storageBucket: "magiccityrecycle.appspot.com",
    messagingSenderId: "325113335418"
  };
  firebase.initializeApp(config);
  db = firebase.firestore();
  auth = firebase.auth();
  const settings = {timestampsInSnapshots: true};
  db.settings(settings);
  auth.onAuthStateChanged(function(u) {
    user = u;
    setupUser();
  });
}

/**
 *  Checks the logged-in user's profile
 * and calls setup*()
 */
function setupUser() {
  if (user == null)
    setupNonLoggedInUser();
  else {
    var docRef = db.collection("users").doc(user.uid);
    docRef.get().then(function(doc) {
      if (doc.exists) {
        if (doc.data().admin) {
          setupAdminUser();
        } else {
          setupRegularUser();
        }
      } else {
        setupRegularUser();
      }
    });
  }
}

/**
 *  Called when we need to setup the UI for the
 * regular, non-logged-in user
 */
function setupNonLoggedInUser() {
  console.log("setting up non-logged in user");
}

/**
 *  Called when we need to setup the UI for a
 * regular logged-in user
 */
function setupRegularUser() {
  console.log("setting up regular in user");
}

/**
 *  Called when we need to setup the UI for a
 * super-admin user
 */
function setupAdminUser() {
  console.log("setting up admin user");
  admin = true;
}

/**
 *  Gets a map of logged-in user info
 * for taggin data with
 */
function getUserInfo() {
  console.log(user);
  if (user)
    return {name: user.displayName, uid: user.uid};
  else
    return {name: "unknown", uid: nil};
}

/**
 *  Determines whether or not someone is the owner
 * of a resource
 */
function isOwner(id) {
  if (user == null)
    return false;
  return user.uid == id || admin;
}

/**
 *  Pulls all of the locations from firestore's
 * /locations/* db. Pass in a function to work
 * with them.
 */
function getLocations(fun) {
  db.collection("locations").get().then(fun);
}

/**
 *  Adds all location markers to the map
 */
function addLocationsToMap(map, locations) {
  locations.forEach((location) => {
    data = location.data();
    if (data.approved || admin || (data.owner && isOwner(data.owner.uid)))
      addLocationToMap(map, location.data(), location.id);
  });
}

/**
 *  Builds some HTML for the popup window that appears
 * above a recycling area
 */
function buildLocationPopupHtml(data, id) {
  var name = "recycle bin";
  var description = null;
  if (data.name) {
    name = data.name;
  }
  if (data.description) {
    description = data.description;
  }
  var str = "<div class='marker-info-styles'><h3>" + name + "</h3>";
  if (admin) {
    str += "<div class='bin-modify'><a href='javascript:deleteLocation(\"" + id + "\")'><span style='color:red;'>delete</span></a>";
    if (!data.approved && data.type != "suggestion")
      str += "<a href='javascript:approveLocation(\"" + id + "\")'>approve</a>";
    str += "</div>";
  } else if (data.type == "suggestion" && isOwner(data.owner.uid)) {
    str += "<div class='bin-modify'><a href='javascript:deleteLocation(\"" + id + "\")'><span style='color:red;'>delete</span></a>";
  }
  if (description != null) {
    str += "</div><p>" + description + "</p>";
    str += "<ul>";
  } else {
    str += "</div>";
  }
  str += "<ul class='marker-info-list'>";
  data.types.forEach((type) => {
    str += "<li>" + type + "</li>";
  });
  str += "</ul>";
  str += "<form action='" + "http://www.google.com/maps/place/" + data.lat + "," + data.lon + "'>";
  str += "<button type='submit'>open in maps</button></form>";
  return str;
}

/**
 *  Adds a location marker to the map
 */
function addLocationToMap(map, data, id) {
  console.log(`adding location ${data.name} to map`);
  console.log(data.location);
  var lnglat = [data.lon, data.lat];

  var el = document.createElement('div');
  el.className = 'marker';
  if (data.type == "recycle_center") {
    el.style.backgroundImage = 'url(https://firebasestorage.googleapis.com/v0/b/magiccityrecycle.appspot.com/o/marker-facility.svg?alt=media&token=53b79bd8-46a5-4010-b1d2-e1bfc35330e6)';
    el.style.width = '60px';
    el.style.height = '75px';
    el.style["background-size"] = '60px 75px';
  } else if (data.type == "bin") {
    el.style.backgroundImage = 'url(https://firebasestorage.googleapis.com/v0/b/magiccityrecycle.appspot.com/o/recycle-bin.svg?alt=media&token=45d6b493-7216-45ca-8e60-0e1762300351)';
    el.style.width = '40px';
    el.style.height = '50px';
    el.style["background-size"] = '40px 50px';
  } else if (data.type == "suggestion") {
    el.style.backgroundImage = 'url(https://firebasestorage.googleapis.com/v0/b/magiccityrecycle.appspot.com/o/recycle-suggestion.svg?alt=media&token=cca0e376-27d0-42d5-a3c4-57d8315bf220)';
    el.style.width = '40px';
    el.style.height = '50px';
    el.style["background-size"] = '40px 50px';
  }

  var txt = buildLocationPopupHtml(data, id);
  el.addEventListener('click', () => {
    var mapEl = $("#map");
    var h = mapEl.height();
    var offset = 0.75 * 0.5 * h;
    map.flyTo({center: [data.lon, data.lat], offset: [0, -offset]});
    showPopup(txt);
  });

  var marker = new mapboxgl.Marker(el)
      .setLngLat(lnglat)
      .addTo(map)
  markers[id] = marker;
}

/**
 *  Adds your current location to the map
 */
function addCurrentLocation(map) {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
      var lnglat = [position.coords.longitude, position.coords.latitude];

      var el = document.createElement('div');
      el.className = 'marker';
      el.style.backgroundImage = 'url(https://firebasestorage.googleapis.com/v0/b/magiccityrecycle.appspot.com/o/mcrecycle-person.svg?alt=media&token=dddc1a8d-3fe3-4184-8f31-8f3e53120f1e)';
      el.style.width = '40px';
      el.style.height = '50px';
      el.style["background-size"] = '40px 50px';

      el.addEventListener('click', () => {
        map.flyTo({center: lnglat});
      });

      var marker = new mapboxgl.Marker(el)
          .setLngLat(lnglat)
          .addTo(map)
      map.flyTo({center: lnglat});
    });
  }
}

/**
 *  Centers the map on the current location
 */
function centerMapOnCurrentLocation(map) {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
      map.jumpTo({center: [position.coords.longitude, position.coords.latitude]});
      map.zoomTo(defaultZoom);
    });
  }
}

/**
 *  Centers the map on the Bham area
 */
function centerMapOnBham(map) {
  map.jumpTo({center: [-86.8208828,33.5125266]});
  map.zoomTo(defaultZoom);
}

/**
 *  Pops-up the info for details about the marker
 * you just clicked on
 */
function showPopup(html) {
  var infoBox = document.querySelector('#popup-info');
  infoBox.style.bottom = '0';
  infoBox.innerHTML = html;
}

/**
 *  Hides the info window at the bottom of the page
 */
function hidePopup() {
  var infoBox = document.querySelector('#popup-info');
  infoBox.style.bottom = '-55vh';
}

/**
 *  Pops-up the info for details about the marker
 * you just clicked on
 */
function showCreateBin(html) {
  var infoBox = document.querySelector('#create-bin');
  infoBox.style.bottom = '0';
}

/**
 *  Hides the info window at the bottom of the page
 */
function hideCreateBin() {
  var infoBox = document.querySelector('#create-bin');
  infoBox.style.bottom = '-45vh';
}

function submitBin() {
  hideCreateBin();
  var lat = submitLocation.lat;
  var lng = submitLocation.lng;
  var recycleTypes = [];
  $('input[name="types"]:checked').each(function() {
    recycleTypes.push(this.value);
  });
  console.log(recycleTypes);
  if (addingBin) {
    console.log("gonna add a bin...");
    addingBin = false;
    addBin(lat, lng, recycleTypes);
  } else if (addingBinRequest) {
    console.log("gonna add a bin request...");
    addingBinRequest = false;
    addBinRequest(lat, lng, recycleTypes);
  }
}

/**
 *  Hides the menu
 */
function toggleMenu() {
  $(".menu-dropdown").toggleClass("showMenu");
  $(".fa-angle-down").toggleClass("rotateIcon");
}

/**
 *  Hides the menu
 */
function hideMenu() {
  $(".menu-dropdown").removeClass("showMenu");
  $(".fa-angle-down").removeClass("rotateIcon");
}

/**
 *  Goes into 'add bin' mode and hides the menu
 */
function addingBinFun() {
  toggleMenu();
  addingBin = true;
}

/**
 *  Goes into 'add bin request' mode and hides the menu
 */
function addingBinRequestFun() {
  toggleMenu();
  addingBinRequest = true;
}

/**
 *  Adds a new bin to the map
 */
function addBin(lat, lon, types) {
  if (!user)
    return;
  var payload = {
    type: "bin",
    lat: lat,
    lon: lon,
    types: types,
    created_at: firebase.firestore.Timestamp.now(),
    approved: false,
    owner: getUserInfo()
  };
  return db.collection('locations').add(payload).then((location) => {
    addLocationToMap(globalMap, payload, location.id);
  });
}

/**
 *  Approves a bin
 */
function approveLocation(id) {
  if (!admin)
    return;
  var batch = db.batch();
  var doc_ref = db.collection("locations").doc(id);
  batch.update(doc_ref, {approved: true});
  batch.commit();
  hidePopup();
}

/**
 *  Requests a new recycling bin
 */
function addBinRequest(lat, lon, types) {
  var payload = {
    name: "Suggested Recycling Location",
    type: "suggestion",
    lat: lat,
    lon: lon,
    types: types,
    created_at: firebase.firestore.Timestamp.now(),
    approved: false,
    owner: getUserInfo()
  };
  return db.collection('locations').add(payload).then((location) => {
    addLocationToMap(globalMap, payload, location.id);
  });
}

/**
 *  delete a bin
 */
function deleteLocation(id) {
  if (!isOwner(id)) {
    console.log("only admins or owners can delete locations!");
    return;
  }
  console.log("deleting " + id);
  hidePopup();
  markers[id].remove();
  return db.collection('locations').doc(id).delete();
}

/**
 *  Creates a new map object
 */
function createMap(container) {
  var map = new mapboxgl.Map({
    container: container,
    style: 'mapbox://styles/mapbox/light-v9',
    center: [-86.8208828,33.5125266],
    zoom: defaultZoom
  });
  globalMap = map;
  hidePopup();
  map.on('movestart', function () {
    hidePopup();
    hideMenu();
  });
  map.on('click', function (e) {
    if (addingBin) {
      console.log("gonna add a bin...");
      submitLocation = e.lngLat;
      showCreateBin()
    } else if (addingBinRequest) {
      console.log("gonna add a bin request...");
      submitLocation = e.lngLat;
      showCreateBin()
    }
  });
  return map;
}
