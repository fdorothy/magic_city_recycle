const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

var db = admin.firestore();
const settings = {timestampsInSnapshots: true};
db.settings(settings);

function addBinImpl(uid, lat, lon, types) {
  var payload = {
    lat: lat,
    lon: lon,
    types: types,
    uid: uid,
    created_at: admin.firestore.Timestamp.now(),
    approved: false
  };
  return db.collection('bins').add(payload);
}

function requestBinImpl(uid, lat, lon, types) {
  var payload = {
    lat: lat,
    lon: lon,
    types: types,
    uid: uid,
    created_at: admin.firestore.Timestamp.now(),
    approved: false
  };
  return db.collection('bin_requests').add(payload);
}

function load_samples() {
  service.addBin("", 33.514987, -86.774125, ["clothes"])
  service.addBin("", 33.521291, -86.730883, ["plastic", "foam", "eggs", "paper"])
  service.addBin("", 33.508018, -86.812173, ["bottles", "cans"])
  service.addBin("", 33.509585, -86.810428, ["bottles", "cans"])
  service.addBin("", 33.510494, -86.808649, ["bottles", "cans"])
  service.addBin("", 33.510176, -86.808588, ["bottles", "cans"])
}

exports.addBin = functions.https.onRequest((req, res) => {
  var data = req.query;
  var result = addBinImpl("", data.lat, data.lon, data.types.split(","), data.uid);
  res.send("added bin");
  return result;
});

exports.requestBin = functions.https.onRequest((req, res) => {
  var data = req.query;
  var result = requestBinImpl("", data.lat, data.lon, data.types.split(","), data.uid);
  res.send("requested bin");
  return result;
});

exports.loadSamples = functions.https.onRequest((req, res) => {
  addBinImpl("", 33.514987, -86.774125, ["clothes"])
  addBinImpl("", 33.521291, -86.730883, ["plastic", "foam", "eggs", "paper"])
  addBinImpl("", 33.508018, -86.812173, ["bottles", "cans"])
  addBinImpl("", 33.509585, -86.810428, ["bottles", "cans"])
  addBinImpl("", 33.510494, -86.808649, ["bottles", "cans"])
  addBinImpl("", 33.510176, -86.808588, ["bottles", "cans"])
  res.send("loaded samples");
  return null;
});

exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});
